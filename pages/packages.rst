.. _packages:

Packages 
========

A package is a file that is deployed to a device. The most common use case is a firmware binary, but it can be any file.

.. figure:: ../assets/screenshots/package_versions.png
   :alt: Packages

   Package Detail View

Handle 
----

Along with a name and description, each package has a ``handle``. The handle is a unique url friendly identifier for the package. It is used in api calls to identify the package.


Versions 
--------

Each Package contains a collection of versions. A version is a specific version of the package that can be deployed to a device. Versions have the following properties:

- **Name** - The version name using `Semantic Versioning <http://semver.org/>`_ i.e. 1.2.3 
- **Release Notes** - Release notes for the version 
- **Track** - The deployment track for the version.

.. note:: By default when a device checks for an update, it will pull the latest version from the ``release`` track, that satisfies the platform/device rules. Versions on other tracks (`beta`, `alpha`) can be requested using the ``track`` query, or by setting a device on a specific track.

