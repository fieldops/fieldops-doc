.. _platforms:

Platforms 
=========

A platform is a type of product or device. 

.. figure:: ../assets/screenshots/platform_view.png
   :alt: Platforms

   Platform Detail View

Handle 
----

Along with a name and description, each platform has a ``handle``. The handle is a unique url friendly identifier for the platform. It is used in api calls to identify the platform.


Package Rules 
-------------

Each platform will have a list of ``Package Rules``. These rules tell ``FieldOps`` which packages are associated with the platform, and which versions are supported by the platform.

The rules use the `Semantic Versioning <https://semver.org/>`_ format.

Example: 

.. code:: js 

    packageRules:
    [
        {package: "main-firmware", rule: ">=1.0.0"},
        {package: "bt-firmware", rule: "2.0.0"},
    ]

In this example we have a platform that supports the ``main-firmware`` package, and any version greater than or equal to ``1.0.0``. It also supports the ``bt-firmware`` package, and only version ``2.0.0``.


Platform Security 
-----------------

Certain Security settings can be applied per Platform. These include: 

    -  **Authentication Scope**: specifies the scope of Authentication required for device actions
        - ``None`` : No Authentication required
        - ``Platform`` : Devices need a valid Platform Authentication Token to interact with the platform. All devices on the platform can use the same token to access the api 
        - ``Device`` : Devices need a valid Device Authentication Token to interact with the platform. Each device needs a unique token (generated with the DeviceId) 