Getting Started 
===============

This guides you through the process of getting started using fieldops for your project. 

Installation 
------------

The recomended method of installing a new isntance is to use docker compose. 

docker-compose.yml:

.. code:: yaml 

    version: "3"

    services:
        mongodb:
            image: mongo:6.0.5
            restart: always

        api:
            image: uprev/fieldops-api
            restart: always
            depends_on:
                - mongodb

        dashboard:
            image: uprev/fieldops-dashboard
            restart: always
            ports:
                - 8080:8080
            depends_on:
                - api


.. code:: bash 

    docker compose up -d


Logging In 
----------

Open the up a browser and navigate to the instance ('http://localhost:8080' if running locally). You will be redirected to the login page. Click 'Sign Up' and create a new account. 


.. image:: ../../assets/screenshots/signup.png
    :width: 100%
    :alt: Sign Up Page

.. note:: The first account created will be automatically approved and given admin level premisions. Subsequent accounts will need to be approved by an admin before they can log in. 


Setting Up Your First Project
-----------------------------

Once you log in you will see the dashboard which can show reports about download/logging activity on your platforms. There is also a list of recent platforms. A `Platform` is just a type of device or product that you want to manage.  

Creating a Platform 
~~~~~~~~~~~~~~~~~~~

1. Click the 'Add Platform' button. Then fill out the details for your new platform. 
2. Fill out the form and click 'Create'

.. image:: ../../assets/screenshots/newplatform.png
    :alt: Add Platform Page


- Name: The name of your platform.
- Handle: A short url-friendly handle for the platform
- Description: A short description of the platform


Turning off Platform Authentication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This step is just for demo purposes to make getting started easier. In a production environment you will want to turn on authentication for your platforms.

1. Click the name of the platform you just created to open the Platform Detail page.
2. Click 'Settings' in the Platform summary 
3. Change Authentication Scope to 'None' and click 'Save'

This will allow you to test the Installation with a web browser without having to worry about authentication. There are 3 authentication available for each platform:

- None: No authentication required. This is useful for testing and development.
- Platform: Platform level authentication tokens are required with requests. This means all devices can use the same platform token to authenticate which makes commissioning new devices very simple
- Device: Each device is issued a device specific token that is used to authenticate requests. This is the most secure option, but requires more work to commission new devices.



Creating Packages 
~~~~~~~~~~~~~~~~~

Click on the platform you just created to open the Platform Detail page. There will be a summary of the platform including a list of rules for which packages are compatible with the platform. A platform can have as many packages associated with it as you want, and packages can be shared by multiple platforms. 

1. Click '+Add Rule'
2. Under the 'Package' dropdown select 'Create New Package' (this will open another form).
3. Fill Out the form and click 'Create' to create a new package. 
4. Set the 'Version Rule' for the new rule 

.. image:: ../../assets/screenshots/newrule.png
    :alt: Add Package Page

In this example, we are saying that the platform has a 'BT Firmware' package, but it must be version 1.0.0 or greater.


.. note:: In the Package Rules table, under 'Latest' it will say 'None'. This is because we have not uplaoded any versions of the package yet. So there are none that match the rule. 

Uploading Package Versions 
~~~~~~~~~~~~~~~~~~~~~~~~~~

Now we will upload a version of our package. Click on the name of the package from the package rules table. This will open the package detail page. (You can also view all pacakges by clicking 'Packages' in top navigation bar).

You will notice that there are 3 tracks listed in the summary ('alpha', 'beta', and 'release'), along with the latest version on each track. By default, when a device requests the latest version, it will pull from the 'release' track. But this can be changed by setting the device to a different track, or adding `track=alpha` to the request url. 

1. Click 'Add New Version'
2. Fill out the form and click 'Create'

.. image:: ../../assets/screenshots/newversion.png
    :alt: Add Package Version Page


- Name: The name of the version using Semantic Versioning. 
- Release Notes: A short description of the changes in this version.
- File: The actual file to be downloaded by the device.
- Track: The track that this version will be created on. This defaults to 'alpha' and can be changed once it is ready for release.


.. note:: Once a version is added you can change the track by click the version in the list, then 'Edit'. This will let you move from 'alpha' to 'beta' to 'release' as testing progresses. 


Testing The platform 
~~~~~~~~~~~~~~~~~~~~

Open a web browser and enter the url for an api call to get the packages for your platform: 

``http://localhost:8080/api/v1/packages?platform={platform_handle}`` 

This will return a list of packages that are compatible with your platform, and there latest version based on platform rules: 

.. code:: json 

    [
        {
            "name": "BT Firmware",
            "handle": "bt",
            "latest": "1.2.0",
        }
    ]

You can download versions by version name, or by using the 'latest' keyword: 

``http://localhost:8080/api/v1/packages/bt/versions/1.2.0`` 
``http://localhost:8080/api/v1/packages/bt/versions/latest?platform={platform_handle}`` 


Managing Devices 
~~~~~~~~~~~~~~~~

Device management is optional, but can be useful for tracking which devices are using which versions of your packages and detecting errors in releases. It really depends on your use case. 

To manage you will need to add a device to your platform. This is automatically done when a device makes a request through the api and provides a device ID that is not already in the system.

1. download the package as the device by replacing the `platform` field with a `device` field in the query. 

``http://localhost:8080/api/v1/packages/bt/versions/latest?device={platform_handle}:{device_id}``

.. note:: The Device ID can be any string that uniquely identifies the device. But it cannot contain slashes, colons, or spaces. When using the device ID in queries it is always passed along with the platform handle. 

2. Navigate back to the Platform detail page. You will see the device is now in the device list. Click on the device to open the device detail page.

Under 'Downloads' you should see the download history for that device, including the download you just performed. On the left is a device Summary which includes the device Track, currently installed packages, and package rules. The package rules are inherited from the platform, but can be overridden for each device. This is meant to allow regression testing on specific devices.


See `Device Actions <file:///C:/uprev/fieldops/fieldops-docker/doc/_build/html/pages/devices.html#actions>`_ for more information on device management and logging

