FieldOps Py 
===========

FieldOps Py is a Python library and CLI for working with the FieldOps API. It can be used through the command line, or as a library for a custom python utility. 

Installation
------------

The tool is deployed with `PyPi <https://pypi.org/project/fieldops/>`_ and can be installed using pip:

.. code:: bash 

    pip install fieldops


Using the CLI 
-------------

The CLI can be used to poll the server for information, download packages, and upload new versions of packages. For the most part it just wraps the rest calls and handles authentication, so the api paths are the same as the rest api.

Show the packages 

.. code:: bash 

    fieldops -u <username> -p <password> -s <server> get packages 

.. note:: You can replace 'get' with 'list' to show a simple list instead of the full detailed json response


Show the details of the latest version of a package: 

.. code:: bash 

    fieldops -u <username> -p <password> -s <server> get packages/pa/versions/latest 

Download the latest version of a package:

.. code:: bash 

    fieldops -u <username> -p <password> -s <server> download packages/pa/versions/latest

Upload a new version of a package:

.. code:: bash 

    fieldops -u <username> -p <password> -s <server> upload packages/pa-firmware -f <path to package file> -v <version number> -r <release notes>

.. note:: you can use the -t or --track flag to specify a track to upload to. If you don't specify a track, the package will be uploaded to 'release' by default.



Using as a library
------------------

The library can be used to create custom provisioning scripts, or to integrate with a CI/CD pipeline.

This example script registers a new 'my_product' device with ID 'SN0001' and requests an access token. The token can then be used by that device for logging, and retrieving updates from FieldOps.

.. code:: python 

    #!/usr/bin/env python3

    from fieldops.fieldopsconn import FieldOpsConn

    fo = FieldOpsConn()
    server = test.field-ops.io
    token = USE_TOKEN_FROM_FIELD_OPS


    #Connect to server 
    fo.connect(host=server,token=token)


    #register a new device and request an auth token for it
    resp = fo.get("/platforms/my_product/devices/SN0001/token").json()

    if 'token' in resp:
        device_token = resp['token']
        #TODO pass token to hardware so it can access the server




.. note:: For information on installation and use, see the `FieldOps Py Package <https://pypi.org/project/fieldops/>`_ and the source on `Gitlab <https://gitlab.com/fieldops/fieldops-py>`_