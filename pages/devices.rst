.. _devices:

Devices 
=======

A device is an actual device (or instance of a platform) deployed in the field. The use of device level features is optional. In its most basic configuration ``FieldOps`` can handle package management at the platform level, with no knowledge of individual devices. However, if you have devices that are deployed in the field, you can use the device level features to manage the devices: 

- **Tracks** : Automated deployment of ``beta`` and ``alpha`` packages to specific devices by setting their ``track`` property
- **Logging** : Logging of device events which can be viewed from the dashboard or queried through the API 
- **State Management** : The ability to set and query ``states`` on devices. This can be used to remotely configure devices, or provide a backup mechanism for device configuration.
- **Data Collection** : The ability for a device to store generic datasets  in the ``FieldOps`` database. This can be used to store device telemetry, or other data that is useful to the platform. 

Device ID 
---------

Each device has a unique ``DeviceId``.This can be in any format but can not contain spaces, and must be unique within the platform. The ``DeviceId`` is used to identify the device in the platform.

Package Rules 
-------------

By default devices inherit the ``Package Rules`` from their platform. But ``Package Rules`` can be overridden at the device level. 


Package Versions 
----------------

Devices can have a list of ``Package Versions``. These are the versions of packages that are currently installed on the device. This is used to track the current versions of packages on the device, and to determine if the device is up to date. This can also be used for error detection and alerting. For example, if a device checks in with a previous version after downloading a newer one, this indicates a problem with the update.


Deployment Track 
----------------

Devices can be individually set to a deployment track. By default when a device downloads an update from the system, it will pull the latest ``release`` version of the package that satisfies the package rules for the platform/device. By setting the track for the device to something else ('beta' or 'alpha') it allows is to pull from other tracks as well. This can be useful for testing new versions of packages before they are released to the entire platform.

.. note:: The tracks are tiered so devices on ``alpha`` can pull from ``alpha``, ``beta``, or ``release``. ``Beta`` will pull from ``beta`` or ``release``, etc. 

Tags 
----

Devices can have a list of ``tags`` which are just string values. These can be used to group devices together, or to provide additional information about the device. They do not serve any functional purpose in the ``FieldOps`` system, but provide a way to add context for any custom tools that would use the API. 


Registering Devices 
-------------------

Devices are automatically registered with the system whenever they perform an action through the API.


Actions 
-------

Devices can perform a number of actions through the API. Some examples are : 


Check-in 
~~~~~~~~

Checking in is not required, but it allows administrators to see the current stats of devices of in the dashboard. 

These examples assume a device with the deviceID: `1233456`. There is no set format for deviceId. It can be a serial number, MAC address, or any other unique identifier that does not contain spaces. 

.. code:: bash 

    PATCH http://localhost:8180/api/v1/platforms/pa/devices/123456
    {
        "packageVersions": {
            "main-firmware": "2.0.0",
            "bootloader": ">=1.0.0"
        }
    }

        

Check Versions 
~~~~~~~~~~~~~~

Devices can check the latest versions of their packages

.. code:: bash 

    GET http://localhost:8180/api/v1/packages?device=pa:1233456

This will return a list of packages and their latest versions based on the package rules for the platform/device. 

.. code:: js 

    [
        { name: "main-firmware", latest: "1.0.0", releaseNotes: "Bug fixes" },
        { name: "bootloader", latest: "2.0.0", releaseNotes: "Bug fixes" },
    ]


Download Package
~~~~~~~~~~~~~~~~

Devices can download a package by specifying the package name and version in the url query, or by using the 'latest' keyword to get the latest version of the package.

.. code:: bash 

    GET http://localhost:8180/api/v1/packages/main-firmware/versions/latest/download?device=pa:1233456

This will download the latest version of the package `main-firmware` that satisfies the package rules for the platform and device 


For more resource constrained systems, packages can also be downloaded in chunks. This allows the device to download the package in smaller chunks, and then reassemble them on the device. This is useful for devices that have limited memory, or for devices that have a slow connection.

.. code:: bash 

    GET http://localhost:8180/api/v1/packages/main-firmware/versions/latest/chunk/0?device=pa:1233456&chunkSize=1000

This will download the first chunk of the latest version of the package `main-firmware` that satisfies the package rules for the platform and device. The chunk size is specified in bytes. If no chunk size is provided, it will default to 1000 bytes.

.. warning:: Chunked downloads have not been implemented yet, but it is next on the list.

.. note:: Downloads are logged on the server, and you can view the download history of any device. Alerts can be set up to notify administrators when a device checks in with a previous version after downloading a newer one, as this indicates a problem with the update.


Logging
~~~~~~~

Devices can log messages to the server. This is useful for debugging and monitoring devices. 

.. code:: bash 

    POST http://localhost:8180/api/v1/platforms/pa/devices/123456/logs
    {
        "level": "error",
        "message": "Problem connecting to on board sensor"
        "tag":"SensorLib",   
    }


Token Requests
~~~~~~~~~~~~~~

If a platform has the Authentication scope set to 'Device' then each device will need its own token to access the API. Device Tokens can be requested through the api by any user with ``device:write`` permissions (by default that is 'admin'  and 'maintainer' roles)

1. Log in as user to get user token 

.. code:: bash 

    POST http://localhost:8180/api/v1/auth/login
    {
        "email": "user_email@example.com",
        "password": "password"
    }

2. Save token from response as `userToken`

3. Request device token using user token

.. code:: bash 

    GET http://localhost:8180/api/v1/platforms/pa/devices/123456/token
    {
        "headers": {
            `Authorization`: `bearer ${userToken}`
        }
    }

4. repeat step 3 for each device that needs a token



.. note:: By default, Device tokens do not expire. 

StateStores 
~~~~~~~~~~~

.. warning:: **Not yet implemented**

Devices can store states on the server. The device can checkin the state when it is modified locally, and periodically check with the api to see if there are changes. This provides a very generic way of adjusting device configurations and settings remotely without the need for a custom administration tool . 

Push current local state for 'mainConfig'

.. code:: bash 

    PATCH http://localhost:8180/api/v1/devices/123456/states/mainConfig
    {
        "led": "on",
        "testMode": "off",
        "name": "My Device"
    }


Check server for changes to 'mainConfig'

.. code:: bash

    GET http://localhost:8180/api/v1/devices/123456/states/mainConfig


DataSets 
~~~~~~~~

.. warning:: **Not yet implemented**

Devices can store named datasets on the server. This is useful for storing device telemetry, or other data that is useful to the platform.

Create a new dataset called 'temperatures' that can store 1000 values 

.. code:: bash 

    PUT http://localhost:8180/api/v1/devices/123456/datasets/temperatures
    {
        "size": 1000
    }

Push new values to dataset 

.. code:: bash 

    POST http://localhost:8180/api/v1/devices/123456/datasets/temperatures/data
    {
        "values": [ 23.4, 23.5, 23.6, 23.7, 23.8 ]
    }

DataSets are not typed, so you can store any type of data in them. 

.. code:: bash 

    POST http://localhost:8180/api/v1/devices/123456/datasets/temperatures/data
    {
        "values": [ { time: 123456789, temp: 23.4 }, { time: 123456790, temp: 23.5}]
    }

.. note:: DataSets are stored in a circular buffer. When the buffer is full, new values will overwrite the oldest values.