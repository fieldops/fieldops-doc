FieldOps Documentation
======================


``FieldOps`` is a device management system that can be used to manage products in the field. It is kept generic and open so that it can be adapted to a variety of use cases. Users can manage the system through a web interface, and devicese can connect using the RestAPI, MQTT, or websocket protocols. The system is designed to be used in a variety of environments, including the cloud, on-premise, or in a hybrid configuration.

``FieldOps`` provides a number of features, including:

- Firmware/Package Management and deployment
- Device Logging
- Remote Device Configuration
- Error Reporting

``FieldOps`` is organized into three main components:

- :ref:`platforms`.
- :ref:`packages`.
- :ref:`devices`.

.. toctree::
    :hidden:
    :maxdepth: 3
    :caption: Overview 
    
    pages/platforms
    pages/packages
    pages/devices



.. toctree::
    :hidden:
    :maxdepth: 3
    :caption: User Guide
    
    pages/guide/guide
    pages/guide/fieldops-py



